const validationFood = new JustValidate(add-food-form,
    {
        validateBeforeSubmitting: true
    })

validationFood

    .addField(name,[
        {
            rule: "required"
        }
    ])
    .addField("#weight",[
        {
            rule: "integer"
        },
        {
            rule: "minNumber",
            value: 5
        }
    ])
    .addField("#price", [
        {
            rule: "number"
        }
    ])
    .onSuccess((event)=>{
        document.getElementById("add-food").get();
    })