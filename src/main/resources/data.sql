CREATE TABLE IF NOT EXISTS department (id INT primary key, department_name varchar(100));

INSERT INTO department(id, name)
VALUES (1, "meats");

INSERT INTO department(id, name)
VALUES (2, "dairy");

INSERT INTO department(id, name)
VALUES (3, "vegetables");

INSERT INTO department(id, name)
VALUES (4, "bakery");

/*CREATE TABLE IF NOT EXISTS employee_table (
    employee_id NOT NULL PRIMARY KEY,
    employee_name VARCHAR(100) NOT NULL,
    dob DATE NOT NULL,
    title VARCHAR(100) NOT NULL,
    department_id int);*/

INSERT INTO employee(employee_id, employee_name, dob, title, department_id)
VALUES(1, "John Doe", "1993-02-27", "Manager", 1);
