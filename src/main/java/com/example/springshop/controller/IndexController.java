package com.example.springshop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

    @GetMapping("/getIndex")
    public ModelAndView getIndex(){
        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }
}
