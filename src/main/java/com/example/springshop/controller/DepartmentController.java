package com.example.springshop.controller;

import com.example.springshop.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DepartmentController {
    @Autowired
    private DepartmentRepository departmentRepository;

    @GetMapping("/departmentList")
    public ModelAndView getAllDepartment(){
        ModelAndView modelAndView = new ModelAndView("list-department");
        modelAndView.addObject("deprtments", departmentRepository.findAll());
        return modelAndView;
    }
}
