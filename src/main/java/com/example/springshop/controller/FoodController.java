package com.example.springshop.controller;

import com.example.springshop.entity.Food;
import com.example.springshop.repository.FoodRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FoodController {
    @Autowired
    private FoodRepository foodRepository;



    @GetMapping("/foodList")
    public ModelAndView getAllFood(){
        ModelAndView modelAndView = new ModelAndView("list-food");
        modelAndView.addObject("foods", foodRepository.findAll());
        return modelAndView;
    }

    /*@GetMapping({"/", "/addFood"})
    public String addFoodForm(Model model){
        model.addAttribute("food", new Food());
        return "add-food-form";
    }

    @RequestMapping(path="/saveFood", method = RequestMethod.POST)
    public String addFood(@Valid Food food, BindingResult bindingResult, Model model){
        System.out.println(food);
        boolean is_errors = bindingResult.hasErrors();
        if(is_errors){
            model.addAttribute("food", food);
            return "add-food-form";
        }
        return "redirect:/foodList";
    }*/

    @GetMapping("/addFood")
    public ModelAndView addFoodForm(){
        ModelAndView modelAndView = new ModelAndView("add-food-form");
        Food newFood = new Food();
        modelAndView.addObject("food", newFood);
        return modelAndView;
    }


    @PostMapping("/saveFood")
    public String addFood(@Valid @ModelAttribute Food food, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "add-food-form";
        }
        else{
        foodRepository.save(food);
        return "redirect:/foodList";}
    }

    @GetMapping("/updateFood")
    public ModelAndView updateFood(@RequestParam Long foodId){
        ModelAndView modelAndView = new ModelAndView("add-food-form");
        Food food = foodRepository.findById(foodId).get();
        modelAndView.addObject(food);
        return modelAndView;
    }

    @GetMapping("/deleteFood")
    public String deleteFood(@RequestParam Long foodId){
        foodRepository.deleteById(foodId);
        return "redirect:/foodList";
    }

}
