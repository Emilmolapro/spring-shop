package com.example.springshop.controller;

import com.example.springshop.entity.Employee;
import com.example.springshop.repository.DepartmentRepository;
import com.example.springshop.repository.EmployeeRepository;
import com.example.springshop.service.DepartmentService;
import com.example.springshop.service.EmployeeService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employeeList")
    public ModelAndView getAllEmployee(){
        ModelAndView modelAndView = new ModelAndView("list-employee");
        modelAndView.addObject("departments", departmentService.findAll());
        modelAndView.addObject("employees", employeeService.findAll());
        return modelAndView;
    }

    @GetMapping("/addEmployee")
    public String addEmployeeForm(Model model){
        model.addAttribute("departments", departmentService.findAll());
        model.addAttribute("employee", new Employee());
        return"add-employee-form";
    }

    /*@GetMapping("/addEmployee")
    public ModelAndView addEmployeeForm(){
        ModelAndView modelAndView = new ModelAndView("add-employee-form");
        Employee newEmployee = new Employee();
        modelAndView.addObject("employee", newEmployee);
        modelAndView.addObject("departments", departmentRepository.findAll());
        return modelAndView;
    }*/

    @PostMapping("/saveEmployee")
    public String addEmployee(@Valid @ModelAttribute Employee employee, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            model.addAttribute("employee",employee);
            model.addAttribute("departments", departmentService.findAll());

            return "add-employee-form";
        }
        else {

            employeeService.save(employee);
            return "redirect:/employeeList";
        }

    }
    @GetMapping("/deleteEmployee")
    public String deleteEmployee(@RequestParam Long employeeId){
        employeeRepository.deleteById(employeeId);
        return "redirect:/employeeList";
    }
    @GetMapping("/updateEmployee")
    public ModelAndView updateEmployee(@RequestParam long employeeId){
        ModelAndView modelAndView = new ModelAndView("add-employee-form");
        Employee employee = new Employee();
        modelAndView.addObject("employee", employee);
        return modelAndView;
    }


}
