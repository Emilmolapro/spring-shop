package com.example.springshop.service;

import com.example.springshop.entity.Employee;
import com.example.springshop.repository.DepartmentRepository;
import com.example.springshop.repository.EmployeeRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    private DepartmentRepository departmentRepository;
    private EmployeeRepository employeeRepository;

    public EmployeeService(@Autowired DepartmentRepository departmentRepository,
                           @Autowired EmployeeRepository employeeRepository){
        this.departmentRepository=departmentRepository;
        this.employeeRepository=employeeRepository;
    }

    public List<Employee> findAll(){return this.employeeRepository.findAll();}

    public Optional<Employee> findById(Long id){return this.employeeRepository.findById(id);}

    @Transactional
    public Employee save(Employee e){return this.employeeRepository.save(e);}

    /*public String getDepartmentName(){
        Long id;
        for(employeeRepository.findById(): findAll()){
            id=departmentRepository.getReferenceById()
        }
        id = departmentRepository.getReferenceById().getDepartmentName()
    }*/
}
