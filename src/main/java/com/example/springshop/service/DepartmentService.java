package com.example.springshop.service;

import com.example.springshop.entity.Department;
import com.example.springshop.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {
    private DepartmentRepository departmentRepository;
    public DepartmentService(@Autowired DepartmentRepository departmentRepository){this.departmentRepository=departmentRepository;}

    public List<Department> findAll(){return this.departmentRepository.findAll();}
    public Optional<Department> findById(Long id){return this.departmentRepository.findById(id);}
}
