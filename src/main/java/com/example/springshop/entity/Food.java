package com.example.springshop.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = Food.TABLE_NAME)
@NoArgsConstructor
@AllArgsConstructor
@Data

public class Food {
    public static final String TABLE_NAME="food_table";
    public static final String FIELD_ID="id";
    public static final String FIELD_NAME="name";
    public static final String FIELD_WEIGHT="weight";
    public static final String FIELD_DESCRIPTION="description";
    public static final String FIELD_PRICE="price";
    public static final String FIELD_EXPIRATION="expiration";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = FIELD_ID, nullable = false, unique = true)
    private Long id;

    @NotEmpty(message = "required field")
    @NotNull(message = "{validation.name.notNull}")
    @Column(name=FIELD_NAME, nullable = false, unique = true)
    private String name;

    @NotNull(message = "required field")
    @Min(value = 0L, message = "value must be positive")
    @Column(name = FIELD_WEIGHT)
    private int weight;
    //weight in grams

    @Size(max = 100, message = "Only up to 100 characters")
    @Column(name = FIELD_DESCRIPTION)
    private String description;

    @Min(value = 0L, message = "value must be positive")
    @NotNull(message = "required field")
    @Column(name = FIELD_PRICE, nullable = false)
    private BigDecimal price;
    //price in huf

    @Column(name = FIELD_EXPIRATION, nullable = true)
    private LocalDate expiration;
}
