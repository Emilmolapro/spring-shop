package com.example.springshop.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = Department.TABLE_NAME)
@NoArgsConstructor
@AllArgsConstructor
public class Department {
    public static final String TABLE_NAME = "department";
    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = FIELD_ID)
    private Long departmentId;

    @NotNull(message = "required field")
    @Column(name = FIELD_NAME)
    private String departmentName;

}
