package com.example.springshop.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.sql.Date;
import java.time.LocalDate;

@Table(name = Employee.TABLE_NAME)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    public static final String TABLE_NAME = "employee";
    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_DOB = "dob";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_DEPARTMENT = "department_id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = FIELD_ID)
    private Long id;

    @NotNull(message = "required field")
    @Column(name = FIELD_NAME, nullable = false)
    private String name;

    @NotNull(message = "required field")
    @Column(name = FIELD_DOB, nullable = false)
    private LocalDate dob;

    @NotNull(message = "required field")
    @Column(name=FIELD_TITLE, nullable = false)
    private String title;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name=FIELD_DEPARTMENT, referencedColumnName = "id", nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Department department;

}
