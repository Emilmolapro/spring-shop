package com.example.springshop.excpetions;

public class NotFoundException extends Exception {

    public NotFoundException(String message) {
        super(message);
    }
}